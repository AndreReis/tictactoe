const TicTacToe = (() => {
  const crossColor = 0xff0000
  const circleColor = 0x0000ff
  
  const finishGame = () => {}
  const rules = Rules(finishGame)

  const tictactoeGame = new Phaser.Game(
    rules.canvasSize,
    rules.canvasSize,
    Phaser.CANVAS,
    'TIC TAC TOE',
    {
      preload,
      create,
      render
    }
  )
  let gameGraphics
  
  function preload () {
    tictactoeGame.input.onUp.add(pointer => {
      rules.play(pointer)
    })
  }
  
  function create () {
    tictactoeGame.stage.backgroundColor = '#fff'
    gameGraphics = tictactoeGame.add.graphics(0, 0)
  }
  
  function render () {
    gameGraphics.clear()
    const gameScore = rules.getGameScore()
    const games = rules.getGames()
    const activeGames = rules.getActiveGames()
    for (let i = 0; i < 3; i++) {
      const row = gameScore[i]
      const gameRow = games[i]
      for (let j = 0; j < row.length; j++) {
        fillSquare(
          row[j],
          {
            x: j,
            y: i
          },
          activeGames[i][j]
        )
        drawGame(
          gameRow[j],
          {
            x: j,
            y: i
          }
        )
      }
    }
    drawGrid(rules.boardSize, rules.boardMargin)
  }

  const drawGrid = (size, margin, pos = { x: 0, y: 0 }) => {
    const xMargin = pos.x * rules.boardSize / 3
    const yMargin = pos.y * rules.boardSize / 3
    gameGraphics.lineStyle(1, 0x000000, 1)
    gameGraphics.moveTo(margin + size / 3 + xMargin, margin + yMargin)
    gameGraphics.lineTo(margin + size / 3 + xMargin, margin + size + yMargin)
    gameGraphics.moveTo(margin + 2 * size / 3 + xMargin, margin + yMargin)
    gameGraphics.lineTo(margin + 2 * size / 3 + xMargin, margin + size + yMargin)
    gameGraphics.moveTo(margin + xMargin, margin + size / 3 + yMargin)
    gameGraphics.lineTo(margin + size + xMargin, margin + size / 3 + yMargin)
    gameGraphics.moveTo(margin + xMargin, margin + 2 * size / 3 + yMargin)
    gameGraphics.lineTo(margin + size + xMargin, margin + 2 * size / 3 + yMargin)
  }
  
  const fillSquare = (side, pos = { x: 0, y: 0 }, active) => {
    if (!rules.testPosition(pos)) {
      return
    }
    
    const colors = [ 0xcccccc, circleColor, crossColor ]
    
    gameGraphics.lineStyle(1, 0x000000, 0)
    gameGraphics.beginFill(colors[side], active && 0.75 || 0.25)
    const squareSize = rules.boardSize / 3
    gameGraphics.drawRect(
      rules.boardMargin + pos.x * squareSize,
      rules.boardMargin + pos.y * squareSize,
      squareSize,
      squareSize)
    gameGraphics.endFill()
    gameGraphics.lineStyle(1, 0x000000, 1)
  }

  const drawGame = (game, pos = { x: 0, y: 0 }) => {
    if (!rules.testPosition(pos)) {
      return
    }

    for (let i = 0; i < game.length; i++) {
      const row = game[i]
      for (let j = 0; j < row.length; j++) {
        row[j] === 1 && drawCircle({
          game: pos, 
          pos: { x: j, y: i }
        }) ||
        row[j] === 2 && drawCross({
          game: pos,
          pos: { x: j, y: i }
        })
      }
    }
    
    drawGrid(
      rules.gameSize,
      rules.boardMargin + rules.gameMargin,
      pos
    )
  }

  const drawCircle = (square) => {
    const coords = rules.coordFromSquare(square)
    gameGraphics.lineStyle(1, circleColor, 1)
    gameGraphics.drawCircle(coords.x, coords.y, 10)
  }

  const drawCross = (square) => {
    const coords = rules.coordFromSquare(square)
    gameGraphics.lineStyle(1, crossColor, 1)
    gameGraphics.drawCircle(coords.x, coords.y, 10)
  }

})()