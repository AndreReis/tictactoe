const Rules = (finishGameCallback) => {
  const canvasSize = 900
  const boardMargin = 50
  const boardSize = canvasSize - 2 * boardMargin
  const gameMargin = 50
  const gameSize = boardSize / 3 - 2 * gameMargin
  const playerScore = [0, 0]
  const gameScore = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
  const games = [
    [
      [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
      [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
      [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    ],
    [
      [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
      [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
      [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    ],
    [
      [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
      [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
      [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    ],
  ]
  let activeGames = [[1, 1, 1], [1, 1, 1], [1, 1, 1]]
  let currentPlayer = 0
  let gameFinished

  const whoWon = (c) =>
    (
      c[0][0] === c[0][1] && c[0][1] === c[0][2] ||
      c[0][0] === c[1][0] && c[1][0] === c[2][0] ||
      c[0][0] === c[1][1] && c[1][1] === c[2][2]
    ) && c[0][0] ||
    (
      c[1][0] === c[1][1] && c[1][1] === c[1][2] ||
      c[0][1] === c[1][1] && c[1][1] === c[2][1] ||
      c[0][2] === c[1][1] && c[1][1] === c[2][0]
    ) && c[1][1] ||
    (
      c[2][0] === c[2][1] && c[2][1] === c[2][2] ||
      c[0][2] === c[1][2] && c[1][2] === c[2][2]
    ) && c[2][2] ||
    0

  const finishGame = () => {
    activeGames = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    gameFinished = true
    finishGameCallback()
  }

  const play = (coords) => {
    if (gameFinished) {
      return
    }

    const square = squareFromCoords(coords)
    if (!square) {
      return
    }

    const game = games[square.game.y][square.game.x]
    if (
      !activeGames[square.game.y][square.game.x] ||
      game[square.pos.y][square.pos.x]
    ) {
      return
    }
    game[square.pos.y][square.pos.x] = currentPlayer + 1
    const score = whoWon(game)
    if (!gameScore[square.game.y][square.game.x] && score) {
      console.log(square.game, score)
      gameScore[square.game.y][square.game.x] = score
      playerScore[score - 1]++
    }
    const winner = whoWon(gameScore)
    if (winner) {
      finishGame()
    } else {
      setActiveGames(square.pos)
      currentPlayer = 1 - currentPlayer
    }
  }

  const squareFromCoords = (coords) => {
    const x1 = (coords.x - boardMargin) / boardSize * 3
    const y1 = (coords.y - boardMargin) / boardSize * 3

    if (!testSquare(x1, y1)) {
      return
    }
    
    const game = {
      x: Math.floor(x1),
      y: Math.floor(y1)
    }

    const x2 = ((x1 - game.x) * boardSize / 3 - gameMargin) / gameSize * 3
    const y2 = ((y1 - game.y) * boardSize / 3 - gameMargin) / gameSize * 3

    if (!testSquare(x2, y2)) {
      return
    }
    
    const pos = {
      x: Math.floor(x2),
      y: Math.floor(y2)
    }

    return {
      game,
      pos
    }
  }

  const coordFromSquare = ({ game, pos }) => ({
    x: boardMargin + boardSize / 3 * game.x + gameMargin + gameSize / 3 * pos.x + gameSize / 6,
    y: boardMargin + boardSize / 3 * game.y + gameMargin + gameSize / 3 * pos.y + gameSize / 6
  })

  const testSquare = (x, y) => x >= 0 && x < 3 && y >= 0 && y < 3

  const testPosition = pos => {
    const possibleValues = [0, 1, 2]
    return possibleValues.includes(pos.x) && possibleValues.includes(pos.y)
  }

  const isFull = game => game.every(row => row.every(cell => cell))

  const setActiveGames = pos => {
    if (!pos) {
      activeGames = [[1, 1, 1], [1, 1, 1], [1, 1, 1]]
      return
    }
    if (!testPosition(pos)) {
      return
    }

    if (isFull(games[pos.x][pos.y])) {
      activeGames = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
      for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
          if (i === pos.x && j === pos.y) continue

          activeGames[i][j] = isFull(games[i][j]) ? 0 : 1
        }
      }
    }
    else {
      activeGames = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
      activeGames[pos.y][pos.x] = 1
    }
  }

  return {
    canvasSize,
    boardSize,
    boardMargin,
    gameSize,
    gameMargin,
    play,
    coordFromSquare,
    testPosition,
    getActiveGames: () => activeGames,
    getGameScore: () => gameScore,
    getGames: () => games
  }
}